import datetime
import os
import cdbwrapc

from cdb.objects import Object, Reference_1, Reference_N, Forward, State, Transition
from cdb.platform import gui
from cdb import util
from cdb import auth
from cdb import ue
from cdb import sqlapi
from mimetools import _counter
from cs.workflow.processes import Process
from cs.workflow.tasks import Task
from cs.workflow import briefcases
from cs.workflow.briefcases import BriefcaseReference
from cdb.objects.cdb_file import CDB_File, FILE_EVENT
from symbol import argument

from cdb import constants
from cdb import kernel
from cdbwrapc import Operation
from cdb.platform import mom

from cs.tools.powerreports import WithPowerReports
from cs.tools.powerreports import ReportTemplateCatalog
from cs.tools import powerreports
from cs.vp.items import Item

from cdb.objects.operations import operation

from cs.pcs.projects import Project 
#from cs.pcs.projects.tasks import Task

#Forward Declarations

fCPQ = Forward(__name__ + ".CPQItem")
fTask = Forward(__name__ + ".CPQTask")
fPTask = Forward("cs.pcs.projects.tasks.Task")
EXCEPTION_MESSAGE = "question_continue_anyway"

class CPQItem(Object, briefcases.BriefcaseContent, WithPowerReports):
    __maps_to__ = "cpq_item"
    __classname__ = "cpq_item"
    
    
    event_map = {('create', 'pre'):("assign_ref_resp"),
                 ('create', 'post'):(("import_browsed_files"),("create_project_from_template"))}
    
    Project=Reference_1(Project, Project.cdb_project_id == fCPQ.cdb_project_id )
    
    
    
#===========================================================================
## STATE DISPATCHING
#===========================================================================
    
    class NEW(State):
        status=0
        def pre(state, self, ctx):
            pass
        def post(state, self, ctx):
            pass
        
#     class QUESTIONSSENTTOCUSTOMER(State):
#         status=10
#         def pre(state, self, ctx):
#             pass
#         def post(state, self, ctx):
#             pass
        
    class QUESTIONSCLARIFIED(State):
        status=30
        def pre(state, self, ctx):
            pass
        def post(state, self, ctx):
            project=self.Project
            tasks=project.Tasks
            clarification_task=tasks[0]
            clarification_task.status=50
            clarification_task.setDone()
            feasibility_task=tasks[4]
            feasibility_task.status=50
            self.create_process_from_template(ctx,'TP000002')
        
    class QUESTIONSNOTCLARIFIED(State):
        status=20
        def pre(state, self, ctx):
            pass
        def post(state, self, ctx):
            project=self.Project
            tasks=project.Tasks
            clarification_task=tasks[0]
            clarification_task.status=50
            clarification_task.setDone()
            feasibility_task=tasks[4]
            feasibility_task.status=50
            self.create_process_from_template(ctx,'TP000002')
            
    class TOBID(State):
        status=50
        def pre(state, self, ctx):
            pass
        def post(state, self, ctx):
            self.create_process_from_template(ctx,'TP000003')
            
    class QUOTATIONSENTTOCUSTOMER(State):
        status=70
        def pre(state, self, ctx):
            pass
        def post(state, self, ctx):
            project=self.Project
            tasks=project.Tasks
            tasks[17].setDone()
            project.ChangeState(200)
            
# Generate Configuration BOM
    def on_cpq_generate_config_bom_now(self,ctx):
        if self.teilenummer not in (sqlapi.NULL,''):
            if self.config_teilenummer in (sqlapi.NULL,''):
                self.copy_item_for_configurable_bom()
            else:
                if self.ask_user(ctx) == True:
                    get_processes = sqlapi.RecordSet2(sql="select cdb_process_id "
                                    "from cdbwf_process_content where cdb_content_id = '%s'"
                                    %(self.cdb_object_id))
                    for each_process in get_processes:
                        pr= Process.ByKeys(each_process.cdb_process_id)
                        if pr.status!=30:
                            pr.cancel_process(comment="New workflow started, "
                                                    "please contact CPQ Manager")
                    existing_config_part =  Item.ByKeys(self.config_teilenummer,self.config_t_index)
                    existing_config_part.ChangeState(50)
                    self.copy_item_for_configurable_bom()
        else:
            raise ue.Exception("cpq_config_error")

    def copy_item_for_configurable_bom(self):
        args = {"teilenummer": "%s_%s-%d"%(self.cpq_id,self.teilenummer,util.nextval("%s-%s"%(self.cpq_id,self.teilenummer))),
                "t_index": self.t_index,
                "cdb_objektart":"cpq_config_part",
                "cdb_classname":"part",
                "status":0,
                "cdb_status_txt":"Active"}
        part_rec = Item.ByKeys(self.teilenummer,self.t_index)
        copied_part =  part_rec.Copy(**args)
        self.config_teilenummer = copied_part.teilenummer
        self.config_t_index =  copied_part.t_index
        #code to copy bom
        for comp in part_rec.Components:
            self.copy_bom(constants.kOperationCopy,
                         comp,
                         baugruppe=copied_part.teilenummer,
                         b_index=copied_part.t_index,
                         position=comp.position,
                         cdb_classname='cpq_config_bom_item')
        

    def ask_user(self, ctx):
        if not EXCEPTION_MESSAGE in ctx.dialog.get_attribute_names(): 
            msg_label = "New CPQ Configurable BOM"
            msg_args = ["Already a configurable view exists, do you want to Obsolete the existing "
                        "configurable BOM ? if Yes, all existing workflows will be cancelled"]
            msgbox = ctx.MessageBox(msg_label,
                                msg_args,
                                EXCEPTION_MESSAGE,
                                ctx.MessageBox.kMsgBoxIconQuestion)
            msgbox.addYesButton(1)
            msgbox.addNoButton()
            ctx.show_message(msgbox)
            # return None
        else:
            result = ctx.dialog[EXCEPTION_MESSAGE]
            if result == ctx.MessageBox.kMsgBoxResultYes:
                return True
            else:
                return False

    def copy_bom(self,operation, cls_or_obj, **args):
        if isinstance(cls_or_obj, Object):
            cls_or_obj = cls_or_obj.ToObjectHandle()

        op = cdbwrapc.Operation(operation,
                                cls_or_obj,
                                mom.SimpleArguments(**args))
        op.run()

        result = op.getObjectResult()
        return result                
#Method to generate reference and response values in the back-end
            
    def assign_ref_resp(self, ctx):
        if ctx.dialog.cpq_id == '#':
            ctx.set('cpq_id', "CPQ%04d"%(util.nextval("cpq_counter")))
        counter_name="ref_counter"
        self.cpq_ref="REF%04d" %(util.nextval(counter_name))
        counter_name2="resp_counter"
        self.cpq_resp="RES%04d" %(util.nextval(counter_name2))
        
#Method to create and initiate the Clarification process post creation of CPQ Item
        
    def on_create_post(self, ctx):
        self.create_process_from_template(ctx,'TP000001')
        ctx.set_followUpOperation("CDB_File.CDB_Create")
        
#Method to import customer input files during CPQ Item creation
                
    def import_browsed_files(self, ctx):
        #if 'localfilename' in ctx.dialog.get_attribute_names():
        if self.import_one:
            filename = self.import_one
        #raise ue.Exception(1024,filename)
        
            from_path=filename
#             stat_data = os.stat(from_path)
#             fdate = datetime.datetime.utcfromtimestamp(stat_data.st_mtime)
#             args = {"cdb_file.cdbf_fdate": fdate,
#                     "cdb_file.cdbf_name": os.path.split(from_path)[1],
#                     "cdb::argument.localfilename": from_path,
#                     "cdb::argument.deletelocalfile": "0"}
#             
#             op_args = {"cdb_file.cdb_object_id": "",
#                        "cdb_file.cdbf_object_id": self.cpq_id,
#                        "cdb_file.cdbf_primary": "0"}
#             args.update(op_args)
#             op = Operation(constants.kOperationNew,
#                            "cdb_file",
#                            mom.SimpleArguments(**args))
#             op.run()
#             result = op.getObjectResult()
#             if not result:
#                 return None
        
            CDB_File.NewFromFile(self.cpq_id, from_path, False)
            
            
#Method to create process from template and activate the created process             
        
    def create_process_from_template(self, ctx, pid):
        pr = Process.CreateFromTemplate(pid,
                                            {"subject_id": auth.persno,
                                             "subject_type": "Person"})
        new_title = "%s (%s)" % (pr.title, self.cpq_id)
        if len(new_title) <= Process.title.length:
            pr.title = new_title
        pr.make_attachments_briefcase()
        pr.AddAttachment(self.cdb_object_id)
        pr.activate_process()
        
#     def display_cpq_overview(self, ctx):
#         #pass
#         ctx.set_followUpOperation('cpq_item_overview',1)  
        
    #def set_project_start_date(self, ctx):
         

    def create_project_from_template(self, ctx):
        template = Project.ByKeys('P000266') 
        pr = "%s (%s)" % ("Project", self.cpq_id)
        counter="PROJECT_ID_SEQ" 
        cdb_proj_id = "P%06d" %util.nextval(counter) 
        attrinfo = { "project_name": pr,"cdb_project_id":cdb_proj_id}
        project = operation("CDB_Copy",template,**attrinfo)
        cpq=self.getPersistentObject()
        cpq.cdb_project_id=project.cdb_project_id
        project.template=0
        project.cdb_classname="cpq_project"
        project.cpq_check=1
        project.ChangeState(50)
        ctx.keep("testing", self.cpq_id)
        ctx.set_followUpOperation("cdbpcs_prj_reset_start_time",keep_rship_context=True,op_object=project,predefined=[])
#         tasks=project.Tasks
#         clarification_task=tasks[0] 
#         clarification_task.status=50
        #clarification_task.setDone()

#Over-writing method of Task class. (To customize the reject paths in the workflows)  

class CPQProject(Project):
    def on_cdbpcs_prj_reset_start_time_post(self, ctx):
        cpq=CPQItem.ByKeys(cdb_project_id = self.cdb_project_id)
        ctx.set_followUpOperation('cpq_item_overview',op_object=cpq)
        
      

class CPQTask(Task):
    
   
    def op_refuse_task(self, ctx):
        
        
        pr=self.Process
        
        reject_tasks_list=["Manufacturing Clarification Review","Engineering Clarification Review","Planning Clarification Review", "Procurement Clarification Review", "Sales Clarification Review"]
        feasibility_task_list=["Manufacturing Feasibility Review","Engineering Feasibility Review","Planning Feasibility Review", "Procurement Feasibility Review"]
        technical_costing_task_list=["Manufacturing Technical Definition Review", "Engineering Technical Definition Review", "Planning Technical Definition Review", "Procurement Technical Definition Review", "Manufacturing Cost Review", "Engineering Cost Review", "Planning Cost Review", "Procurement Cost Review"]
        
#Reject path for Clarification workflow review tasks        
        
        
        if (pr.title.startswith("Clarification") or pr.title.startswith("CPQ")) and (self.title in reject_tasks_list or self.title in technical_costing_task_list):
            self.status=0
            restart_task_title=self.title[:-7]
            if self.title in reject_tasks_list:
                seq_task_groups=pr.TaskGroups[0].TaskGroups
            else:
                seq_task_groups=pr.TaskGroups[1].TaskGroups
            for group in seq_task_groups:
                tasks=group.Tasks
                for task in tasks:
                    if task.title==restart_task_title:
                        restart_task=task
                        if restart_task:
                            restart_task.status=10
                            
#Reject path for Clarification workflow approval task
                            
#         elif pr.title.startswith("Clarification") and self.title =="Approve Prepared Questions":
#             self.status=0
#             tasks=pr.Tasks
#             current_task_index=tasks.index(self)
#             execution_restart_task=tasks[current_task_index-1]
#             execution_restart_task.status=10
            
#Reject path for Clarification workflow approval task governing the status change
            
        elif pr.title.startswith("Clarification") and self.title =="CPQ Approval Condition":
            self.status=30
            questions_not_clarified_task=pr.TaskGroups[1].Tasks[0]
            questions_not_clarified_task.activate_task()
            questions_clarified_task=pr.TaskGroups[1].Tasks[1]
            questions_clarified_task.status=35
            
#Reject path for the main CPQ Workflow feasibility review tasks
            
        elif pr.title.startswith("CPQ") and self.title in feasibility_task_list:
            counter_name=pr.title+self.title
            task_counter="%d" %(util.nextval(counter_name))
            count=task_counter
            if count=="0":
                self.status=0
                restart_feasibility_title=self.title[:-6]+"Study"
                feasibility_task_groups=pr.TaskGroups[0].TaskGroups
                for feasibility_group in feasibility_task_groups:
                    tasks=feasibility_group.Tasks
                    for task in tasks:
                        if task.title==restart_feasibility_title:
                            restart_feasibility_task=task
                            restart_feasibility_task.status=10
                
            elif count=="1":
                sql="""delete from cdb_counter where counter_name='%s'""" \
                    % (counter_name)
                rset=sqlapi.RecordSet2(sql=sql)
                if self.Process.Briefcases[0].Content:
                    cpq =self.Process.Briefcases[0].Content[0]
                    project=cpq.Project
                    project.status=180
                
                self.Super(CPQTask).op_refuse_task(ctx)
                
            else:
                task_counter="%d" %(util.nextval(counter_name))                #change this to relevant code later like log or print
                
#Reject path for the main CPQ Workflow technical definitions and costing review tasks
                
#         elif pr.title.startswith("CPQ") and self.title in technical_costing_task_list:
#             self.status=0
#             restart_task_title=self.title[:-7]
#             Taskgroup=pr.TaskGroups[1].TaskGroups
#             for seq_task_group in Taskgroup:
#                 tasks=seq_task_group.Tasks
#                 for task in tasks:
#                     if task.title==restart_task_title:
#                         restart_task=task
#                         restart_task.status=10
                        
#Reject path for the main CPQ Workflow final quotation review task
                        
        elif (pr.title.startswith("CPQ") or pr.title.startswith("Clarification")) and (self.title=="Final Quotation Review" or self.title == "Review Quotation" or self.title =="Approve Prepared Questions"):
            self.status=0
            tasks=pr.Tasks
            review_task_index=tasks.index(self)
            execution_restart_task=tasks[review_task_index-1]
            execution_restart_task.status=10
            
        elif pr.title.startswith("CPQ") and self.title =="CPQ Approve Condition":
            self.status=30
            cancel_task_list=[pr.TaskGroups[2].Tasks[0], pr.TaskGroups[2].Tasks[1]]
            CPQ_Lost_task=pr.TaskGroups[2].Tasks[2]
            CPQ_Lost_task.activate_task()
            for task in cancel_task_list:
                task.status=35
          
#Super class method to be called for the remaining processes.
                                                           
        else:
            self.Super(CPQTask).op_refuse_task(ctx)
            
    def op_close_task(self, ctx):
             
        pr=self.Process
        feasibility_task_list=["Manufacturing Feasibility Review","Engineering Feasibility Review","Planning Feasibility Review", "Procurement Feasibility Review"]
        technical_task_list=["Manufacturing Technical Definition Review", "Engineering Technical Definition Review", "Planning Technical Definition Review", "Procurement Technical Definition Review"]
        costing_task_list=["Manufacturing Cost Review", "Engineering Cost Review", "Planning Cost Review", "Procurement Cost Review"] 
#Condition where the main CPQ Workflow feasibility review tasks are accepted (for the second time)        
        
        if pr.title.startswith("CPQ") and self.title in feasibility_task_list:
            if self.Process.Briefcases[0].Content:
                cpq =self.Process.Briefcases[0].Content[0]
                project=cpq.Project
                tasks=project.Tasks
            
                for task in tasks:
                    if task.task_name == self.title:
                        task.status=50
                        task.setDone()
                        if not pr.feasibility_counter:
                            pr.feasibility_counter=0
                        pr.feasibility_counter = pr.feasibility_counter+1
                        
                if pr.feasibility_counter == 4:
                    tech_definition_task=tasks[9]
                    tech_definition_task.status=50
                    
                        
                        
                
                
                
                
#             tasks[1].setDone()
#             tasks[3].setDone()
            counter_name=pr.title+self.title
            sql="""delete from cdb_counter where counter_name='%s'""" \
                % (counter_name)
            rset=sqlapi.RecordSet2(sql=sql)
            self.Super(CPQTask).op_close_task(ctx)
            
#Condition where the CPQ status has to be made On Hold            
            
        elif pr.title.startswith("CPQ") and self.title == "CPQ Approve Condition" and self.on_hold == 1:
            status_tasks=pr.TaskGroups[2].Tasks
            self.status=20
            for task in status_tasks:
                if task.title == "CPQ On Hold":
                    #task.status=20
                    task.activate_task()
                else:
                    task.status=35
#             self.Process.Briefcases[0].Content[0].status=100
#             self.Process.Briefcases[0].Content[0].cdb_status_txt="On Hold"
#             self.Process.status=20

        elif pr.title.startswith("CPQ") and self.title in technical_task_list:
            if self.Process.Briefcases[0].Content:
                cpq =self.Process.Briefcases[0].Content[0]
                project=cpq.Project
                tasks=project.Tasks
                
#                 tech_definition_task=tasks[9]
#                 tech_definition_task.status=50
            
                for task in tasks:
                    if task.task_name == self.title:
                        task.status=50
                        task.setDone()
                        #pr.feasibility_counter = pr.feasibility_counter+1
                        
#                 if pr.feasibility_counter == 8:
#                     cost_evaluation_task=tasks[14]
#                     cost_evaluation_task.status=50
                        
            self.Super(CPQTask).op_close_task(ctx)
            
            
        elif pr.title.startswith("CPQ") and self.title in costing_task_list:
            if self.Process.Briefcases[0].Content:
                cpq =self.Process.Briefcases[0].Content[0]
                project=cpq.Project
                tasks=project.Tasks
                
#                 tech_definition_task=tasks[9]
#                 tech_definition_task.status=50
                cost_evaluation_task=tasks[14]
                if not cost_evaluation_task.status == 50:
                    cost_evaluation_task.status = 50
                for task in tasks:
                    if task.task_name == self.title:
                        task.status=50
                        task.setDone()
                        if not pr.cost_counter:
                            pr.cost_counter=0
                        pr.cost_counter = pr.cost_counter+1
                         
                if pr.cost_counter == 4:
                    pricing_task=tasks[16]
                    pricing_task.status=50
                        
            self.Super(CPQTask).op_close_task(ctx)
            
        elif pr.title.startswith("CPQ") and self.title == "Final Quotation Review":
            if self.Process.Briefcases[0].Content:
                cpq =self.Process.Briefcases[0].Content[0]
                project=cpq.Project
                tasks=project.Tasks
                tasks[16].setDone()
                tasks[17].status=50
                
            self.Super(CPQTask).op_close_task(ctx)
                
            
            
             
            
        
            
                   
                
        
        else:
            self.Super(CPQTask).op_close_task(ctx)
            
class CPQReport(powerreports.CustomDataProvider):
    CARD = powerreports.N
    CALL_CARD = powerreports.CARD_N

    def getData(self, parent_result, source_args, **kwargs):
        pass
    def getSchema(self):
        pass
    

                
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
             
                 
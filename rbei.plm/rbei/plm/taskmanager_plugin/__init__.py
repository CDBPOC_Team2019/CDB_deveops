#!/usr/bin/env python
# -*- mode: python; coding: iso-8859-1 -*-
#
# Copyright (C) 1990 - 2014 CONTACT Software GmbH
# All rights reserved.
# http://www.contact.de/
#

from cs.workflow.tasks import Task
from cdb.objects import Object
from cdb import classbody
from cdb import sig
from cdb import misc
from cdb.objects.org import Person


@classbody.classbody
class Task(Object):
    @sig.connect(Task, "create", "pre")
    @sig.connect(Task, "copy", "pre")
    def update_doc_attrs(self,ctx):
		print "xyz"
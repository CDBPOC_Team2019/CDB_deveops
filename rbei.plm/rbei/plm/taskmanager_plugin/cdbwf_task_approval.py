#!/usr/bin/env python
# -*- python -*- coding: iso-8859-1 -*-
#
# Copyright (C) 1990 - 2012 CONTACT Software GmbH
# All rights reserved.
# http://www.contact.de/

__docformat__ = "restructuredtext en"
__revision__ = "$Id: cdbwf_task_approval.py 127524 2015-07-07 13:58:08Z via $"


from cdb import sig
from cdb import util

from cs.workflow.taskgroups import TaskGroup
from cs.workflow.tasks import ApprovalTask
from cs.workflow.taskmanager_plugin import common
from cs.workflow.taskmanager_plugin.cdbwf_task_approval import get_plugin
from cs.workflow.taskmanager_plugin.cdbwf_task_approval import TaskManagerPlugin 

__all__ = []


class CustomPlugin(TaskManagerPlugin):

    __plugin_macro_file__ = "cdbwf_task_approval.html"

    @classmethod
    def change_task(cls, task_obj, action, value, comment):

        result = None
        success = True

        if action == 'change_status':
            if not cls.is_reserved_by_other(task_obj):
                try:
                    if value == 'ok':
                        task_obj.CloseTask(remark=comment)
                    elif value == 'nok':
                        task_obj.RefuseTask(remark=comment)
                    elif value == 'onHold':
                        task_obj.Update(on_hold=1)
                        task_obj.CloseTask(remark=comment)

                    # Weiterleiten
                    else:
                        task_obj.ForwardTask(remark=comment)
                except Exception, e:
                    result = (unicode(e)
                              .replace("\\n", "\n")
                              .replace("\\t", "\t")
                              .replace("\\\\", "\\"))
                    success = False
            else:
                success = False
                result = unicode(
                    util.ErrorMessage("cdbwf_task_reserved_by_other"))
        elif action == 'reserve_task':
            task_obj.Reload()
            try:
                task_obj.setReserved()
            except Exception, e:
                result = (unicode(e)
                          .replace("\\n", "\n")
                          .replace("\\t", "\t")
                          .replace("\\\\", "\\"))
                success = False
        elif action == 'onHold_check':
            if task_obj.title == "CPQ Approve Condition":
                result = "enable"
            else:
                result = "disable"
        return result, success

    @classmethod
    def get_attr_of_info_overlay(cls, task_obj, is_login_user):
        result = super(TaskManagerPlugin, cls).get_attr_of_info_overlay(
            task_obj, is_login_user)

        forward_label = ""
        if task_obj.isForwardable():
            from cdb.platform.gui import Message
            mynext = task_obj.Next
            if isinstance(mynext, TaskGroup):
                forward_label = "%s '%s'" % (
                    Message.GetMessage("cdbwf_forward1"), mynext.title)
            else:
                forward_label = "%s %s" % (
                    Message.GetMessage("cdbwf_forward2"),
                    task_obj.Next.getSubjectName())

        result["forward_label"] = unicode(forward_label)

        return result


# lazy initialization
app = None

sig.disconnect(get_plugin) 

@sig.connect(ApprovalTask, "cs.taskmanager.getplugins")
def get_plugin():
    global app
    if app is None:
        app = CustomPlugin()
    return app

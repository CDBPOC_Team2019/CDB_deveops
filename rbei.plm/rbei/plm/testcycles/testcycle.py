#!/usr/bin/env powerscript
# -*- mode: python; coding: iso-8859-1 -*-

from cdb import sqlapi,ue,cad,util
from cdb.objects import Object
from cs.vp.items import Item
from cdb import auth
import re

#custom class creation for TESTPLAN class
class TESTLAB_testcycle(Object):

    __maps_to__   = "testlab_test_cycle"
    __classname__ = "testlab_test_cycle"
    
    event_map = {
        # pre_mask
        # post_mask
        (('create', 'copy'), 'post_mask'): ('create_cycle_id',),
        # pre
        # post
        # now
        # dialogitem_change
        }
        
    def create_cycle_id(self, ctx):
        startValue = 0
        
        #checking existence of TESTCYCLE_NR from cdb_counter
        rec = sqlapi.RecordSet2(sql="select * from cdb_counter where counter_name='TESTCYCLE_NR'")
        if not rec:
            util.nextval("TESTCYCLE_NR")
            #creating new record in cdb_counter table.
            sqlapi.SQLupdate("cdb_counter set counter_curr = %d where counter_name='TESTCYCLE_NR'" %(int(startValue)))
            test_cycle_id = "TC%06d" %startValue
        else:
            test_cycle_id = "TC%06d" % (util.nextval("TESTCYCLE_NR"))
            
        self.test_cycle_id = test_cycle_id

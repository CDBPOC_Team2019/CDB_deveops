#!/usr/bin/env powerscript
# -*- mode: python; coding: iso-8859-1 -*-

from cdb import sqlapi,ue,cad,util
from cdb.objects import Object
from cs.vp.items import Item
from cdb import auth
import re
from cs.workflow.processes import Process
from rbei.plm.testcycles.testcycle import TESTLAB_testcycle
from rbei.plm.testplans.testplan import TESTLAB_testplan

#custom class creation for TESTPLAN class
class TESTLAB_test(Object):

    __maps_to__   = "testlab_test"
    __classname__ = "testlab_test"

    event_map = {
        # pre_mask
        (('create', 'copy', 'modify'), ('pre_mask')): ('set_part_number'), 
        # post_mask
        (('create', 'copy'), ('post_mask')): ('ttlb_setNumber'),
        # pre
        # post
        # now
        # dialogitem_change
        (('create', 'copy'), ('dialogitem_change')): ('change_customer_type'),
        (('create', 'copy', 'modify'), ('dialogitem_change')): ('part_number'),
    }
    
        
    def ttlb_setNumber(self,ctx):
        test_id = ctx.dialog['test_cycle_id']
        prefSet = sqlapi.RecordSet2("prefixes",
                                        "prefix='%s'" % test_id,
                                        updatable=1)
        if not prefSet:
            curSeq = 1
            sqlapi.SQLinsert("into prefixes (prefix,seq) values ('%s',%s)"
                             % (test_id, 2))
        else:
            curSeq = prefSet[0].seq
            prefSet[0].update(seq=(curSeq + 1))

        result = "%s-%04d" % (test_id, curSeq)
        ctx.set('testlab_test_id', result)
        
    def set_part_number(self, ctx):
        cycle_id = ctx.dialog['test_cycle_id']
        if cycle_id:
            test_cycle = TESTLAB_testcycle.ByKeys(cycle_id)
            plan_id =  test_cycle.test_plan_id
            test_plan = TESTLAB_testplan.ByKeys(plan_id)
            if test_plan:
                part = test_plan.teilenummer
                ctx.set('teilenummer', part)
        
    def change_customer_type(self, ctx):
        if ctx.changed_item == "test_customer_type":
            if ctx.dialog['test_customer_type'] == 'External Test':
                ctx.set("org_id", '')
                ctx.set_writeable("org_id")
            elif ctx.dialog['test_customer_type'] == 'Internal Test':
                ctx.set("org_id", '')
                ctx.set_readonly("org_id")
            
    def part_number(self, ctx):
        if ctx.changed_item == "test_cycle_id":
            cycle_id = ctx.dialog['test_cycle_id']
            if cycle_id:
                test_cycle = TESTLAB_testcycle.ByKeys(cycle_id)
                plan_id =  test_cycle.test_plan_id
                test_plan = TESTLAB_testplan.ByKeys(plan_id)
                if test_plan:
                    part = test_plan.teilenummer
                    ctx.set('teilenummer', part)
    
    def on_testlab_test_initiate_testing_now(self,ctx):
        if self.cdb_classname == "testlab_emission":
            process_id = "P00000000"
        elif self.cdb_classname == "testlab_performance":
            process_id = "P00000001"
        pr = Process.CreateFromTemplate(process_id,
                                        {"cdb_project_id": "",
                                         "subject_id": auth.persno,
                                         "subject_type": "Person"})
        pr.make_attachments_briefcase()
        pr.AddAttachment(self.cdb_object_id)
        for each_task in pr.AllTasks:
            if each_task.title in ('Emission Test','Performance Test'):
                each_task.subject_id =  self.test_engineer
                each_task.subject_type = "Person"
        pr.activate_process()
        self.ChangeState(25)

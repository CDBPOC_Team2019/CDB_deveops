#!/usr/bin/env powerscript
# -*- coding: iso-8859-1 -*-

from cdb import sqlapi, misc, ue
from cs.tools import powerreports as PowerReports

from rbei.plm.testcycles.testcycle import TESTLAB_testcycle
from rbei.plm.testplans.testplan import TESTLAB_testplan
from rbei.plm.tests.test import TESTLAB_test

class TestPlanReport(PowerReports.CustomDataProvider):
    CARD = PowerReports.CARD_N
    CALL_CARD = PowerReports.CARD_N

    def getData(self, parent_result, source_args, **kwargs):
        result = PowerReports.ReportDataList(self)
        for test_plan in parent_result:
            plan = test_plan.getObject()
            plan_id = plan.test_plan_id
            part_number = plan.teilenummer
            plan2cycles = sqlapi.RecordSet2(sql="select * from testlab_test_cycle where test_plan_id = '%s'" % plan_id)
            count_tp= 0
            count_tc=0
            for plan2cycle in plan2cycles:
                #r1 = PowerReports.ReportData(self, plan)
                cycle_id = plan2cycle.test_cycle_id
                cycle_name = plan2cycle.test_cycle_name
                cycle2tests = sqlapi.RecordSet2(sql="select * from testlab_test where test_cycle_id = '%s'" % plan2cycle.test_cycle_id)
                for cycle2test in cycle2tests:
                    r = PowerReports.ReportData(self, plan)
                    r["test_plan_id"] = plan_id
                    r["teilenummer"] = part_number
                    r["test_cycle_id"] = cycle_id
                    r["test_cycle_name"] = cycle_name
                    r["testlab_test_id"] = cycle2test.testlab_test_id
                    r["test_start_date"] = cycle2test.test_start_date
                    r["test_end_date"] = cycle2test.test_end_date
                    r["testlab_test_type"] = cycle2test.testlab_test_type
                    r["test_customer_type"] = cycle2test.test_customer_type
                    result += r
        return result

    def getSchema(self):
        t = PowerReports.XSDType(self.CARD)
        t.add_attr("test_plan_id", sqlapi.SQL_CHAR)
        t.add_attr("teilenummer", sqlapi.SQL_CHAR)
        t.add_attr("test_cycle_id", sqlapi.SQL_CHAR)
        t.add_attr("test_cycle_name", sqlapi.SQL_CHAR)
        t.add_attr("testlab_test_id", sqlapi.SQL_CHAR)
        t.add_attr("test_start_date", sqlapi.SQL_CHAR)
        t.add_attr("test_end_date", sqlapi.SQL_CHAR)
        t.add_attr("testlab_test_type", sqlapi.SQL_CHAR)
        t.add_attr("test_customer_type", sqlapi.SQL_CHAR)
        return t
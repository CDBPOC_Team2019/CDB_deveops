#!/usr/bin/env powerscript
# -*- mode: python; coding: iso-8859-1 -*-

from cdb import sqlapi,ue,cad,util
from cdb.objects import Object
from cs.vp.items import Item
from cdb import auth
import re
from cs.tools.powerreports import WithPowerReports

#custom class creation for TESTPLAN class
class TESTLAB_testplan(Object, WithPowerReports):

    __maps_to__   = "testlab_test_plan"
    __classname__ = "testlab_test_plan"

    event_map = {
        (('create', 'copy'), ('post_mask')): ('ttlb_setNumber'), 
    }
    
    #setting number with format    
    def ttlb_setNumber(self,ctx):
        startValue = 0        
        #checking existence of TESTPLAN_NR from cdb_counter
        rec = sqlapi.RecordSet2(sql="select * from cdb_counter where counter_name='TESTPLAN_NR'")
        if not rec:
            util.nextval("TESTPLAN_NR")
            #creating new record in cdb_counter table.
            sqlapi.SQLupdate("cdb_counter set counter_curr = %d where counter_name='TESTPLAN_NR'" %(int(startValue)))
            test_plan_id = "TP%06d" %startValue
        else:
            test_plan_id = "TP%06d" % (util.nextval("TESTPLAN_NR"))
            
        self.test_plan_id = test_plan_id
        teilenummer = self.teilenummer
        sqlapi.SQLupdate("teile_stamm set test_plan_id = '%s' where teilenummer='%s'" %(self.test_plan_id,teilenummer))
#!/usr/bin/env powerscript
# -*- mode: python; coding: utf-8 -*-

from cs.workflow.tasks import ApprovalTask
from cs.workflow.tasks import ExaminationTask
from cs.workflow.tasks import Task
from cs.workflow.processes import Process
from cdb.objects import State
from datetime import datetime
from cdb import util
from cs.workflow.forms import Form
from cs.pcs.projects import SubjectAssignment
from cdb.objects.org import Person


class TestLabTask(Task):
    
    class COMPLETED(Task.COMPLETED):
        def pre(state, self, ctx):
            super(Task.COMPLETED, state).pre(self, ctx)
            self.check_form_data()
            form_data= ''
            for form in self.EditForms:
                form_data = form.read_data()
                test_count = len((self.Process.Briefcases)[0].Content)
                if test_count>0:
                    for each_test in (self.Process.Briefcases)[0].Content:
                        if "TESTLAB" in str(each_test.__class__):
                            if each_test.testlab_test_type == 'Emission Test':
                                if form_data:
                                    testlab_emission_vehicle_type = str(form_data['testlab_emission_vehicle_type'])
                                    testlab_emission_co =  str(form_data['testlab_emission_co'])
                                    testlab_emission_hc =  str(form_data['testlab_emission_hc'])
                                    if str(form_data['start_date']):
                                        start_date = datetime.strptime(str(form_data['start_date']), u"%m/%d/%Y")
                                    else:
                                        start_date = ""
                                    if str(form_data['end_date']):
                                        end_date = datetime.strptime(str(form_data['end_date']), u"%m/%d/%Y")
                                    else:
                                        end_date = ""
                                    if 2.01 <= float(testlab_emission_co) <= 2.03 and 1.1 <= float(testlab_emission_hc) <= 1.13:
                                        test_result = "Pass"
                                        test_analysis = "All Parameter are within range"
                                    else:
                                        test_result = "Fail"
                                        if 2.01 <= float(testlab_emission_co) <= 2.03:
                                            test_analysis = "HC values are not within the range (1.1-1.13)"
                                        elif 1.1 <= float(testlab_emission_hc) <= 1.13:
                                            test_analysis = "CO% values are not within the range (2.01-2.03)"
                                        else:
                                            test_analysis = "HC values are not within the range (1.1-1.13) \nCO% values are not within the range (2.01-2.03)"
                                    each_test.Update(testlab_emission_vehicle_type=testlab_emission_vehicle_type,
                                                    testlab_emission_co=testlab_emission_co,
                                                    testlab_emission_hc=testlab_emission_hc,
                                                    test_start_date =  start_date,
                                                    test_end_date = end_date,
                                                    test_result = test_result,
                                                    test_analysis = test_analysis )
                                    each_test.ChangeState(100)
                            elif each_test.testlab_test_type == 'Performance Test':
                                    if form_data:
                                        test_perf_accer_measured_time = str(form_data['test_perf_accer_measured_time'])
                                        test_perf_accer_perf_goal =  str(form_data['test_perf_accer_perf_goal'])
                                        test_perf_accer_peak_power =  str(form_data['test_perf_accer_peak_power'])
                                        test_perf_speed_quarter = str(form_data['test_perf_speed_quarter'])
                                        test_perf_speed_mile =  str(form_data['test_perf_speed_mile'])
                                        test_perf_speed_perf_goal =  str(form_data['test_perf_speed_perf_goal'])
                                        if str(form_data['start_date']):
                                            start_date = datetime.strptime(str(form_data['start_date']), u"%m/%d/%Y")
                                        else:
                                            start_date = ""
                                        if str(form_data['end_date']):
                                            end_date = datetime.strptime(str(form_data['end_date']), u"%m/%d/%Y")
                                        else:
                                            end_date = ""
                                        if 200 <= int(test_perf_accer_peak_power) <= 210 and 60 <= float(test_perf_speed_mile) <= 62:
                                            test_result = "Pass"
                                            test_analysis = "All Parameter are within range"
                                        else:
                                            test_result = "Fail"
                                            if 200 <= float(test_perf_accer_peak_power) <= 210:
                                                test_analysis = "Peak power values are not within the range (200-210)"
                                            elif 60 <= float(test_perf_speed_mile) <= 62:
                                                test_analysis = "Speed values after 1Mile are not within the range (60-62)"
                                            else:
                                                test_analysis = "Peak power values are not within the range (200-210) \nSpeed values after 1Mile are not within the range (60-62)"
                                        each_test.Update(test_perf_accer_measured_time=test_perf_accer_measured_time,
                                                        test_perf_accer_perf_goal=test_perf_accer_perf_goal,
                                                        test_perf_accer_peak_power=test_perf_accer_peak_power,
                                                        test_perf_speed_quarter=test_perf_speed_quarter,
                                                        test_perf_speed_mile=test_perf_speed_mile,
                                                        test_perf_speed_perf_goal=test_perf_speed_perf_goal,
                                                        test_start_date =  start_date,
                                                        test_end_date = end_date,
                                                        test_result = test_result,
                                                        test_analysis = test_analysis )
                                        each_test.ChangeState(100)
                                        

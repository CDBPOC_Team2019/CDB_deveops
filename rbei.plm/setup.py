from cdb.comparch.pkgtools import setup

setup(
    name="rbei.plm",
    version="1.0.0",
    install_requires=['cs.platform', 'cs.web', 'cs.base', 'cs.designsystem', 'cs.activitystream', 'cs.launchpad', 'cs.officelink', 'cs.taskboard', 'cs.powerreports', 'cs.taskmanager', 'cs.workflow', 'cs.actions', 'cs.metrics', 'cs.documents', 'cs.portfolios', 'cs.defects', 'cs.pcs', 'cs.vp', 'cs.bomcreator', 'cs.ec', 'cs.vp-pcs', 'cs.workspaces', 'cscdb.product'],
    docsets=[
        # Add a relative path for each documentation set in this package
        ],
    cdb_modules=[
        # List the package's modules in the correct (initialization) order as
        # computed by cdb.comparch topological sort. This list goes into
        # `cdb_modules.txt` in the EGG-INFO.
        "rbei.plm"
        ],
    cdb_services=[
        # List the services of this packages by their class names. This list
        # goes into `cdb_services.txt` in EGG-INFO.
        ],
)
